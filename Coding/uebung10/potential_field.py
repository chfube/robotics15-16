from graphics import *
from random import randint as rand
from math import isinf

# size of the window
x_size_win = 500
y_size_win = 500

# size of the actual coordinate system
x_size = 100
y_size = 100

# parameters for color generation
std_dev_mult = 2.0
lower_bound = 0.3
upper_bound = 0.9

# avg force calculation parameters
min_force_for_avg = 0
max_force_for_avg = 5000

# potential scale parameters
attr_pot_scale = 0.1
rep_pot_scale = 100.0

# random walk length
random_walk_length = 1

# number of obstacles
num_obstacles = 8


# Converts a point-tuple into a graphics.Point
def point(point_tuple):
    return Point(point_tuple[0], point_tuple[1])


# Scales a point-tuple from the field values to the window pixel values
def scale_to_win(point_tuple):
    return int(1.0 * point_tuple[0] / x_size * x_size_win) - 1, \
           int(1.0 * point_tuple[1] / y_size * y_size_win) - 1


# Scales a point-tuple from the window pixel size to the field values
def scale_from_win(point_tuple):
    return 1.0 * point_tuple[0] / x_size_win * x_size + 1, \
           1.0 * point_tuple[1] / y_size_win * y_size + 1


# Calculates the attraction of the goal point
def calc_goal_force(pos, pos_goal, scale=1.0):
    return scale * ((pos[0] - pos_goal[0]) ** 2 + (pos[1] - pos_goal[1]) ** 2)


# Calculates the repulsion of the obstacle
def calc_obstacle_force(pos, obstacles, scale=1.0):
    max_force = 0
    for o in obstacles:
        if pos == o:
            return float('inf')
        force = scale / ((pos[0] - o[0]) ** 2 + (pos[1] - o[1]) ** 2)
        if force > max_force:
            max_force = force
    return max_force


# Returns a color for the given force value
def get_force_color(force, avg, std_dev):
    min_force = max(avg - std_dev_mult * std_dev, 0)
    max_force = avg + std_dev_mult * std_dev
    if force > max_force:
        return color_rgb(255, 0, 0)
    if force < min_force:
        return color_rgb(0, 255, 0)

    scale = 1.0 * (force - min_force) / (max_force - min_force)

    if scale < lower_bound:
        scale /= lower_bound
        return color_rgb(scale * 255, 255, 0)
    elif scale > upper_bound:
        scale -= upper_bound
        scale /= (1 - upper_bound)
        return color_rgb(255, (1 - scale) * 136, 0)
    else:
        scale -= lower_bound
        scale /= (upper_bound - lower_bound)
        return color_rgb(255, scale * 136 + (1 - scale) * 255, 0)


# Builds a 2D-array potential field for the given goal and obstacles
def build_potential_field(goal, obstacles):
    potential_field = [[0 for _ in range(y_size_win)] for _ in range(x_size_win)]

    for x in range(x_size_win):
        for y in range(y_size_win):
            p = scale_from_win((x, y))
            force = calc_goal_force(p, goal, scale=attr_pot_scale)
            force += calc_obstacle_force(p, obstacles, scale=rep_pot_scale)
            potential_field[x][y] = force

    return potential_field


# Draws the given 2D-array potential field into the given window
def draw_potential_field(win, potential_field):
    avg = 0
    count = 0

    for x in range(x_size_win):
        for y in range(y_size_win):
            force = potential_field[x][y]
            if not isinf(force) and min_force_for_avg < force < max_force_for_avg:
                avg += force
                count += 1.0

    avg /= count

    std_dev = 0

    for x in range(x_size_win):
        for y in range(y_size_win):
            force = potential_field[x][y]
            if not isinf(force) and min_force_for_avg < force < max_force_for_avg:
                std_dev += abs(potential_field[x][y] - avg)

    std_dev /= count

    print("Average force: " + str(avg))
    print("Standard deviation: " + str(std_dev))

    img = Image(Point(x_size_win / 2, y_size_win / 2), x_size_win, y_size_win)
    for x in range(x_size_win):
        for y in range(y_size_win):
            force = potential_field[x][y]
            img.setPixel(x, y, get_force_color(force, avg, std_dev))

    img.draw(win)


# Finds the path from start to goal using the given potential field
def find_path(start, goal, potential_field):
    path = [start]
    current_pos = start
    current_potential = float('inf')
    visited = [start]
    while current_pos != goal:
        neighbors = get_neighbors(current_pos)
        min_potential = float('inf')
        min_neighbor = None
        for n in neighbors:
            scaled_n = scale_to_win(n)
            potential = potential_field[scaled_n[0]][scaled_n[1]]
            if potential < min_potential:
                min_potential = potential
                min_neighbor = n

        if min_potential < current_potential or not (min_neighbor in visited):
            current_potential = min_potential
            current_pos = min_neighbor
            visited.append(current_pos)
            path.append(current_pos)
        else:
            # perform a random walk
            for i in range(random_walk_length):
                neighbors = get_neighbors(current_pos)
                index = rand(0, len(neighbors) - 1)
                n = neighbors[index]
                scaled_n = scale_to_win(n)
                current_potential = potential_field[scaled_n[0]][scaled_n[1]]
                current_pos = n
                path.append(current_pos)

    path = remove_duplicates(path)

    return path


# Clears the path of duplicate point and removes the unnecessary parts
def remove_duplicates(path):
    i = 0
    while i < len(path):
        node = path[i]
        if node in path[i + 1:]:
            index = path[i + 1:].index(node)
            path = path[:i + 1] + path[i + index + 2:]
        i += 1

    return path


# Returns all neighbor-points of the given position
def get_neighbors(pos):
    neighbors = []
    if pos[0] > 1:
        neighbors.append((pos[0] - 1, pos[1]))
        if pos[1] > 1:
            neighbors.append((pos[0] - 1, pos[1] - 1))
        if pos[1] < y_size:
            neighbors.append((pos[0] - 1, pos[1] + 1))
    if pos[0] < x_size:
        neighbors.append((pos[0] + 1, pos[1]))
        if pos[1] > 1:
            neighbors.append((pos[0] + 1, pos[1] - 1))
        if pos[1] < y_size:
            neighbors.append((pos[0] + 1, pos[1] + 1))
    if pos[1] > 1:
        neighbors.append((pos[0], pos[1] - 1))
    if pos[1] < y_size:
        neighbors.append((pos[0], pos[1] + 1))
    return neighbors


# Draws the given path into the given window
def draw_path(win, path):
    for i in range(len(path) - 1):
        p1 = point(scale_to_win(path[i]))
        p2 = point(scale_to_win(path[i + 1]))
        line = Line(p1, p2)
        line.setOutline('magenta')
        line.draw(win)


# Draws a point-tuple containing field values into the given window
def draw_scaled_point(win, point_tuple, color):
    scaled_point = point(scale_to_win(point_tuple))
    circle = Circle(scaled_point, 5)
    circle.setFill(color)
    circle.setOutline(color)
    circle.draw(win)


def main():
    win = GraphWin("Potential Field", x_size_win, y_size_win)

    start = (rand(1, x_size), rand(1, y_size))
    goal = (rand(1, x_size), rand(1, y_size))
    obstacles = [(rand(1, x_size), rand(1, y_size)) for _ in range(num_obstacles)]

    print("Start Point: " + str(start))
    print("Goal Point: " + str(goal))
    print("Obstacles: " + str(obstacles))

    potential_field = build_potential_field(goal, obstacles)
    draw_potential_field(win, potential_field)

    draw_scaled_point(win, start, 'cyan')
    draw_scaled_point(win, goal, 'blue')

    path = find_path(start, goal, potential_field)
    print("Path: " + str(path))
    draw_path(win, path)

    win.getMouse()
    win.close()


if __name__ == "__main__":
    main()
