from graphics import *
import random
import math
import sys

dimx = 1000
dimy = 1000

win = GraphWin('Voronoi', dimx, dimy)
img = Image(Point(int(dimx / 2), int(dimy / 2)), dimx, dimy)


# Berechnet die Distanz zwischen zwei Punkten
def dist(p1, p2):
    return math.sqrt((p1[0] - p2[0]) ** 2 + (p1[1] - p2[1]) ** 2)


# Zeichnet einen Pixel in ein Bild.
def drawPixel(img, x, y, r, g, b):
    img.setPixel(x, y, color_rgb(r, g, b))


colors = [(255, 0, 0), (204, 102, 0), (255, 255, 0), (102, 255, 51), (0, 255, 204), (0, 102, 204), (153, 102, 255),
          (255, 0, 255), (255, 0, 102), (102, 102, 153)]


# Berechnet eine Liste von zufälligen punkten anhand von dimx und dimy.
# Sollten count > 10 gewählt werden, so sollte colors um count-10 einträge erweitert werden
def randPoints(count):
    pointDict = {}
    while len(pointDict) < count:
        x = random.randint(1, 100)
        y = random.randint(1, 100)
        key = str(x) + str(y)
        if key in pointDict:
            continue
        color_index = len(pointDict)
        pointDict[key] = (x, y, colors[color_index])
    return pointDict


# Ermittelt alle Punkte die den kleinsten Abstand zu x,y haben.
def getNearestList(x, y, pointDict):
    points = pointDict.items()
    dists = []
    # Berechne Distanzen
    for point in points:
        d = dist((x, y), point[1])
        dists.append((d, point[1]))
    mindist = float('inf')
    nearest = []
    # Berechne die kleinste Distanz
    for q in dists:
        d = q[0]
        if d <= mindist:
            mindist = d
    # Nehme alle Punkte in das Ergebnis auf welche die mindist besitzen
    nearest = [q for q in dists if q[0] == mindist]
    return nearest


# Erstellt ein Netz aus Voronoizellen. Verwendet für die Berechnung Werte zwischen 1, 1.1 ,...100.0
def voronoi():
    pointDict = randPoints(10)
    for x in range(10, 1001):
        for y in range(10, 1001):
            nearest = getNearestList(x * 0.1, y * 0.1, pointDict)
            if len(nearest) == 1:
                r = ((nearest[0][1])[2])[0]
                g = ((nearest[0][1])[2])[1]
                b = ((nearest[0][1])[2])[2]
                drawPixel(img, x, y, r, g, b)
            else:
                drawPixel(img, x, y, 0, 0, 0)


voronoi()
img.draw(win)
win.getMouse()
win.close()
