class GridNode:
    """
    x and y coordinate in the grid.
    optional: succ_filter, function that can filters the base list of
    successors (e.g. obstacles).
    """
    def __init__(self, x, y, succ_filter=None):
        self.x = x
        self.y = y
        self.succ_filter = succ_filter

    def successors(self):
        successors = [GridNode(self.x - 1, self.y, self.succ_filter),
                      GridNode(self.x + 1, self.y, self.succ_filter),
                      GridNode(self.x, self.y - 1, self.succ_filter),
                      GridNode(self.x, self.y + 1, self.succ_filter)]
        if self.succ_filter is not None:
            self.succ_filter(successors)

        return successors

    def __eq__(self, other):
        return self.x == other.x and self.y == other.y

    def __str__(self):
        return '(' + str(self.x) + ', ' + str(self.y) + ')'

    def __repr__(self):
        return str(self)

    def __hash__(self):
        return hash(str(self))
