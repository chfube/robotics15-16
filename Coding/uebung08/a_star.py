"""
Class implementing the A-Star algorithm
"""
class AStar:
    """
    cost and estimate are functions taking two nodes as parameters.

    cost returns the actual costs of the edge between two nodes.

    estimate returns the estimation of the costs between two nodes.
    """
    def __init__(self, cost, estimate):
        self.cost = cost
        self.estimate = estimate
        self.open_list = []
        self.closed_list = []
        self.g = dict()
        self.pred = dict()
        self.f = dict()
        self.target_node = None

    """
    Returns the path calculated by A-Star from the start-node to the
    target-node.

    The nodes have to be comparable (__eq__ and __hash__) and need to
    implement a successors() function returning the successors of the
    node.
    """
    def a_star(self, start_node, target_node):
        self.open_list = [start_node]
        self.closed_list = []
        self.g = dict()
        self.g[start_node] = 0
        self.pred = dict()
        self.pred[start_node] = None
        self.f = dict()
        self.f[start_node] = 0
        self.target_node = target_node

        while self.open_list:
            current_node = self.remove_min()

            if current_node == target_node:
                return self.calculate_path()

            self.closed_list.append(current_node)

            self.expand_node(current_node)

        return []

    def remove_min(self):
        min_node = self.open_list[0]
        for node in self.open_list:
            if self.f[node] < self.f[min_node]:
                min_node = node
        self.open_list.remove(min_node)
        return min_node

    def expand_node(self, node):
        for successor in node.successors():
            if successor in self.closed_list:
                continue

            new_g = self.g[node] + self.cost(node, successor)

            if successor in self.open_list and new_g > self.g[successor]:
                continue

            self.pred[successor] = node
            self.g[successor] = new_g

            self.f[successor] = new_g + self.estimate(successor, self.target_node)

            if successor not in self.open_list:
                self.open_list.append(successor)

    def calculate_path(self):
        path = [self.target_node]
        while self.pred[path[0]] is not None:
            path.insert(0, self.pred[path[0]])
        return path
