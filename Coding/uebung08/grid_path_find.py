from a_star import AStar
from grid_node import GridNode

obstacle_pos = 1


def cost(node1, node2):
    return 1


def estimate(node1, node2):
    return abs(node1.x - node2.x) + abs(node1.y - node2.y)


def succ_filter(successors):
    for node in successors[:]:
        if node.y == 5 and 3 + obstacle_pos <= node.x <= 7 + obstacle_pos:
            successors.remove(node)

if __name__ == '__main__':
    find = AStar(cost, estimate)
    start_node = GridNode(5, 1, succ_filter)
    target_node = GridNode(5, 10, succ_filter)
    print(find.a_star(start_node, target_node))
