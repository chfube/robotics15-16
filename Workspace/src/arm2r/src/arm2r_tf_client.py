#!/usr/bin/env python
__author__ = 'christoph'

from arm2r.srv import *
import rospy
import sys

if __name__ == "__main__":
    rospy.wait_for_service('tf_service')
    if len(sys.argv) == 2:
        try:
            transform = rospy.ServiceProxy('tf_service', TransMsg)
            resp1 = transform(sys.argv[1])
            print(resp1.result)
        except rospy.ServiceException, e:
            print "Service call failed: %s"%e
