#!/usr/bin/env python

import rospy
from sensor_msgs.msg import JointState
import tf
import math


# generate a JointState message for both joints
def gen_jointstate_msg(rot_first, rot_second):
    msg = JointState()
    msg.header.stamp = rospy.Time(0)
    msg.name.append("joint_base_first")
    msg.name.append("joint_first_second")
    msg.position.append(rot_first)
    msg.position.append(rot_second)
    return msg


def arm2r_tf_listener():
    rospy.init_node('arm2r_tf_listener', anonymous=True)

    # init the publisher for the joint states
    pub = rospy.Publisher('/calibrated/joint_states', JointState, queue_size=10)

    # init the transform listener
    tf_listener = tf.TransformListener()

    # define our node refresh rate
    rate = rospy.Rate(10)

    # open file to save the distance log
    dist_file = open('/home/benjamin/Documents/Uni/Robotics/distance.txt', 'w')

    time = 0
    x = 0

    while not rospy.is_shutdown():
        # rotation of the first joint
        rot_first = math.sin(4 * x)
        # rotation of the second joint
        rot_second = -rot_first + math.pi / 2
        # generate joint state message for rotations
        msg = gen_jointstate_msg(rot_first, rot_second)
        # publish the message to the topic
        pub.publish(msg)
        x += 0.1

        try:
            # position of end_link relative to base_link
            pos, _ = tf_listener.lookupTransform("end_link", "base_link", rospy.Time(0))
            # calculate euclidean distance
            distance = math.sqrt(math.pow(pos[0], 2) + math.pow(pos[1], 2) + math.pow(pos[2], 2))
            # if not logged enough, write distance stats to file
            if time < 50:
                dist_file.write(str(time) + " " + str(distance) + "\n")
            time += 1
        except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException):
            pass

        rate.sleep()

    dist_file.close()


if __name__ == '__main__':
    arm2r_tf_listener()
