#!/usr/bin/env python

import rospy
import tf
from sensor_msgs.msg import JointState
import numpy as np
from numpy.linalg import inv
from math import sin, cos

out_path = '/tmp/'

L1 = 1.0
L2 = 0.9
pub = None
tf_listener = None


# Iterate in T steps to target point x_target with initial
# joint state q_0. Optionally write trajectory coordinates
# into outfile.
def inverse_kinematics(q_0, T, x_target, outfile=None):
    rate = rospy.Rate(15)
    q = q_0
    x_0 = phi_pos()
    for t in [i + 1.0 for i in range(T)]:
        x = phi_pos()
        if outfile is not None:
            outfile.write(str(x[0][0]) + " " + str(x[1][0]) + "\n")
        j_sharp = build_j_sharp(q[0][0], q[1][0])
        x_new = np.add(x_0, np.multiply((t / T), np.subtract(x_target, x_0)))
        q = np.add(q, np.dot(j_sharp, np.subtract(x_new, x)))
        pub.publish(gen_jointstate_msg(q[0][0], q[1][0]))
        rate.sleep()
    return q


# generate a JointState message for both joints
def gen_jointstate_msg(rot_first, rot_second):
    msg = JointState()
    msg.header.stamp = rospy.Time(0)
    msg.name.append("joint_base_first")
    msg.name.append("joint_first_second")
    msg.position.append(rot_first)
    msg.position.append(rot_second)
    return msg


# Calculates the inverse Jacobi-Matrix
def build_j_sharp(t_1, t_2):
    j = np.array([[-L1 * cos(t_1) - L2 * cos(t_2 + t_1), -L2 * cos(t_2 + t_1)],
                  [-L1 * sin(t_1) - L2 * sin(t_2 + t_1), -L2 * sin(t_2 + t_1)]])
    return inv(j)


# Returns the current end-effector position
def phi_pos():
    pos, _ = tf_listener.lookupTransform("base_link", "end_link", rospy.Time(0))
    return np.array([[pos[1]], [pos[2]]])


def arm2r_inverse_kinematic():
    global tf_listener, pub

    # init the publisher for the joint states
    pub = rospy.Publisher('/calibrated/joint_states', JointState, queue_size=10)

    # init the transform listener
    tf_listener = tf.TransformListener()

    # define our node refresh rate
    rate = rospy.Rate(1)

    # Define a starting position
    q_0 = np.array([[0], [-0.5890309702]])

    rospy.loginfo("Init for: " + str(q_0[0][0]) + " " + str(q_0[1][0]));

    # Wait for the transform to be available
    started = False
    while not started:
        try:
            phi_pos()
            started = True
        except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException):
            pass

        rate.sleep()

    # Move into the start position
    pub.publish(gen_jointstate_msg(q_0[0][0], q_0[1][0]))

    rate.sleep()

    target_x = 0.5
    target_y = 0.5
    period = 1
    repeat = 0

    steps = 100

    outfile = open(out_path + 'trajectory' + str(steps) + '.txt', 'w')
    while repeat <= 4:
        if repeat == 0:
            q_0 = inverse_kinematics(q_0, 100, np.array([[target_x], [target_y]]))
        else:
            q_0 = inverse_kinematics(q_0, steps, np.array([[target_x], [target_y]]), outfile)
        if period == 0:
            target_x = -target_x
        else:
            target_y = -target_y
        period = (period + 1) % 2
        repeat += 1
        rate.sleep()
    outfile.close()


if __name__ == "__main__":
    rospy.init_node('arm2r_inverse_kinematic', anonymous=True)
    arm2r_inverse_kinematic()
