#!/usr/bin/env python
__author__ = 'christoph'

from math import pi

from arm2r.srv import *
import rospy
from sensor_msgs.msg import JointState


# generate a JointState message for both joints
def gen_jointstate_msg(rot_first, rot_second):
    msg = JointState()
    msg.header.stamp = rospy.Time(0)
    msg.name.append("joint_base_first")
    msg.name.append("joint_first_second")
    msg.position.append(rot_first)
    msg.position.append(rot_second)
    return msg


def transform(req):
    if req.requestedstate == 'LEFT':
        msg = gen_jointstate_msg(pi * 0.375, pi * 0.125)
        pub.publish(msg)
        return TransMsgResponse("OK")
    elif req.requestedstate == 'CENTER':
        msg = gen_jointstate_msg(0, 0)
        pub.publish(msg)
        return TransMsgResponse("OK")
    elif req.requestedstate == 'RIGHT':
        msg = gen_jointstate_msg(-pi * 0.375, -pi * 0.125)
        pub.publish(msg)
        return TransMsgResponse("OK")
    return TransMsgResponse("UNKNOWN MESSAGE TYPE!!!")


def tf_server():
    rospy.init_node('tf_server')
    s = rospy.Service('tf_service', TransMsg, transform)
    rospy.spin()


if __name__ == "__main__":
    # init the publisher for the joint states
    pub = rospy.Publisher('/calibrated/joint_states', JointState, queue_size=10)
    tf_server()
