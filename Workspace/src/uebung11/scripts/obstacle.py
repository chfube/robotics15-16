class Obstacle:
    def __init__(self, x, y, width, height):
        self.pos = (x, y)
        self.size = (width, height)
