# coding=utf-8
import math


def intersects_obstacle(start_pos, end_pos, obstacle):
    """
    Überprüft, ob eine Strecke das Hindernis schneidet.
    :param start_pos: Start der Strecke
    :param end_pos:  Ende der Strecke
    :param obstacle: Hindernis
    """
    min_y = min(start_pos[1], end_pos[1])
    max_y = max(start_pos[1], end_pos[1])

    min_x = min(start_pos[0], end_pos[0])
    max_x = max(start_pos[0], end_pos[0])
    if start_pos[0] == end_pos[0]:
        # Check Left and Right
        if obstacle.pos[0] <= start_pos[0] <= obstacle.pos[0] + obstacle.size[0]:

            # Check Bottom
            if min_y <= obstacle.pos[1] <= max_y:
                return True
            # Check Top
            if min_y <= obstacle.pos[1] + obstacle.size[1] <= max_y:
                return True
    else:
        f = linear_function(start_pos, end_pos)

        # Check Left
        if min_x <= obstacle.pos[0] <= max_x and obstacle.pos[1] <= f(obstacle.pos[0]) <= obstacle.pos[1] + \
                obstacle.size[1]:
            return True

        # Check Right
        if min_x <= obstacle.pos[0] + obstacle.size[0] <= max_x and obstacle.pos[1] <= f(
                        obstacle.pos[0] + obstacle.size[0]) <= obstacle.pos[1] + obstacle.size[1]:
            return True

    if start_pos[1] == end_pos[1]:
        # Check Top and Bottom
        if obstacle.pos[1] <= start_pos[1] <= obstacle.pos[1] + obstacle.size[1]:
            # Check Left
            if min_x <= obstacle.pos[0] <= max_x:
                return True
            # Check Right
            if min_x <= obstacle.pos[0] + obstacle.size[0] <= max_x:
                return True
    else:
        f = invert_linear_function(start_pos, end_pos)

        # Check Bottom
        if min_y <= obstacle.pos[1] <= max_y and obstacle.pos[0] <= f(obstacle.pos[1]) <= obstacle.pos[0] + \
                obstacle.size[0]:
            return True

        # Check Top
        if min_y <= obstacle.pos[1] + obstacle.size[1] <= max_y and obstacle.pos[0] <= f(
                        obstacle.pos[1] + obstacle.size[1]) <= obstacle.pos[0] + obstacle.size[0]:
            return True

    return False


def linear_function(start_pos, end_pos):
    """
    Berechnet die lineare Funktion durch start_pos und end_pos
    """
    slope = 1.0 * (end_pos[1] - start_pos[1]) / (end_pos[0] - start_pos[0])
    intercept = start_pos[1] - slope * start_pos[0]
    return lambda x: slope * x + intercept


def invert_linear_function(start_pos, end_pos):
    """
    Berechnet die Umkehrfunktion zur linearen Funktion durch start_pos und end_pos
    """
    slope = 1.0 * (end_pos[0] - start_pos[0]) / (end_pos[1] - start_pos[1])
    intercept = start_pos[0] - slope * start_pos[1]
    return lambda x: slope * x + intercept


class WayNode:
    def __init__(self, position, obstacles=None):
        if obstacles is None:
            obstacles = []
        self.position = position
        self.obstacles = obstacles

    def get_direction_vector(self, target_node):
        x = target_node.position[0] - self.position[0]
        y = target_node.position[1] - self.position[1]
        norm = math.sqrt(x ** 2 + y ** 2)
        return x / norm, y / norm

    def get_next_node(self, step_width, target_node):
        """
        Gibt den Knoten an, der um step_width entfernt in Richtung target_node liegt.
        """
        direction_vector = self.get_direction_vector(target_node)
        direction_vector_x = direction_vector[0] * step_width
        direction_vector_y = direction_vector[1] * step_width
        new_x = self.position[0] + direction_vector_x
        new_y = self.position[1] + direction_vector_y
        return WayNode((new_x, new_y), obstacles=self.obstacles)

    def is_valid(self, parent):
        """
        Gibt an, ob es sich um einen gültigen Punkt handelt (Nicht im Hindernis)
        """
        for o in self.obstacles:
            if intersects_obstacle(parent.position, self.position, o):
                return False
        return True

    def distance(self, other):
        return math.sqrt((self.position[0] - other.position[0]) ** 2 +
                         (self.position[1] - other.position[1]) ** 2)

    def __eq__(self, other):
        return self.position == other.position

    def __str__(self):
        return str(self.position)

    def __repr__(self):
        return str(self)

    def __hash__(self):
        return hash(str(self))
