# coding=utf-8
from rrt_node import RRTNode


def get_path(target_node):
    """
    Bestimmt mithilfe von Zurücktraversierung den Pfad von der Wurzel zu target_node.
    :param target_node: Endknoten des Pfades
    """
    path = [target_node.configuration]
    node = target_node
    while node.parent is not None:
        node = node.parent
        path.insert(0, node.configuration)
    return path


class RRT:
    def __init__(self, gen_random_node, step_width, goal_distance, goal_sample_prob):
        """
        Initialisiert die RRT-Klasse.

        :param gen_random_node (goal, goal_probability): Funktion zur Generierung der zufälligen Knoten
        :param step_width: Schrittweite in Richtung des zufälligen Knotens
        :param goal_distance: In welcher Distanz das Ziel als erreicht gilt
        :param goal_sample_prob: Mit welcher Wahrscheinlichkeit (in Prozent) das Ziel gesampled wird
        """
        self.rrt_graph_root = None
        self.gen_random_node = gen_random_node
        self.step_width = step_width
        self.goal_distance = goal_distance
        self.goal_sample_prob = goal_sample_prob
        return

    def rrt(self, start, goal):
        """
        Findet einen Pfad mithilfe von RRT von start nach goal.
        :param start: Startpunkt
        :param goal: Zielpunkt
        """
        self.rrt_graph_root = RRTNode(None, start)

        new_configuration = start
        new_node = self.rrt_graph_root
        while goal.distance(new_configuration) > self.goal_distance:
            random_node = self.gen_random_node(goal, self.goal_sample_prob)
            nearest = self.nearest_node(self.rrt_graph_root, random_node)[0]
            new_configuration_test = nearest.configuration.get_next_node(self.step_width, random_node)
            if not new_configuration_test.is_valid(nearest.configuration):
                continue
            new_configuration = new_configuration_test
            new_node = RRTNode(nearest, new_configuration)
            nearest.children.append(new_node)

        return get_path(new_node)

    def nearest_node(self, current_node, node_to_find):
        """
        Bestimmt den nächsten Knoten zu node_to_find im Teilbaum unter current_node.

        :param current_node: Der zu untersuchende Knoten
        :param node_to_find: Der zu findende Knoten
        """
        min_node = current_node
        min_dist = current_node.configuration.distance(node_to_find)

        for node in current_node.children:
            out = self.nearest_node(node, node_to_find)
            if out[1] < min_dist:
                min_node = out[0]
                min_dist = out[1]

        return min_node, min_dist
