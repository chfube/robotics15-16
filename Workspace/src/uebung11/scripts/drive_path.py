#!/usr/bin/env python
# coding=utf-8

from random import randint
from way_node import WayNode
from obstacle import Obstacle
from rrt import RRT

import rospy
from nav_msgs.msg import Path
from geometry_msgs.msg import PoseStamped, PolygonStamped, Point32


def gen_random_node(goal, goal_probability):
    """
    Mit einer Wahrscheinlichkeit von goal_probability wird
    goal zurückgegeben, ansonsten wird ein Knoten im Bereich
    x,y in [-500,500] zufällig generiert.
    """
    if randint(1, 100) <= goal_probability:
        return goal
    x = randint(-500, 500)
    y = randint(-500, 500)
    return WayNode((x, y))


def convert_path(node_path):
    """
    Wandelt einen Knoten-Pfad in einen ROS-Pfad um
    """
    out_path = Path()
    poses = []
    for node in node_path:
        next_node = PoseStamped()
        next_node.pose.position.x = node.position[0]
        next_node.pose.position.y = node.position[1]

        next_node.pose.orientation.x = 0
        next_node.pose.orientation.y = 0
        next_node.pose.orientation.z = 1
        next_node.pose.orientation.w = 0

        poses.append(next_node)

    out_path.poses = poses
    out_path.header.stamp = rospy.Time(0)
    out_path.header.frame_id = 'map'
    return out_path


def tuple_to_point(tuple_point):
    """
    Wandelt ein Tupel in einen Point32 um
    """
    point = Point32()
    point.x = tuple_point[0]
    point.y = tuple_point[1]
    point.z = 0
    return point


def publish_obstacles(pub, obstacles):
    """"
    Published die Hindernisse im Topic (Zur Anzeige in rviz)
    """
    for o in obstacles:
        polygon = PolygonStamped()
        polygon.polygon.points.append(tuple_to_point(o.pos))
        polygon.polygon.points.append(tuple_to_point((o.pos[0] + o.size[0], o.pos[1])))
        polygon.polygon.points.append(tuple_to_point((o.pos[0] + o.size[0], o.pos[1] + o.size[1])))
        polygon.polygon.points.append(tuple_to_point((o.pos[0], o.pos[1] + o.size[1])))
        polygon.polygon.points.append(tuple_to_point(o.pos))
        polygon.header.stamp = rospy.Time(0)
        polygon.header.frame_id = '/map'
        pub.publish(polygon)


def save_as_gnu_plot(filepath, node_list):
    """
    Speichert die Koordinaten der Knoten in der angegebenen Datei
    """
    f = open(filepath, 'w')
    for node in node_list:
        f.write(str(node.position[0]) + ' ' + str(node.position[1]) + '\n')
    f.close()


def main():
    pub = rospy.Publisher('/path', Path, queue_size=10, latch=True)
    obst_pub = rospy.Publisher('/obst', PolygonStamped, queue_size=10, latch=True)
    rospy.init_node('PathPlanner', anonymous=True)

    obstacles = [Obstacle(75, -25, 25, 125)]
    publish_obstacles(obst_pub, obstacles)
    rrt = RRT(gen_random_node, 5.0, 2.5, 90)
    start = WayNode((0, 0), obstacles=obstacles)
    goal = WayNode((150, 75), obstacles=obstacles)

    path = rrt.rrt(start, goal)
    save_as_gnu_plot("/home/benjamin/Dokumente/Uni/robotics15-16/path.txt", path)

    pub.publish(convert_path(path))
    rospy.spin()


if __name__ == '__main__':
    main()
