#!/usr/bin/env python
# coding=utf-8

import rospy
from nav_msgs.msg import Odometry, Path
from ackermann_msgs.msg import AckermannDriveStamped
from math import pi, atan, sqrt, copysign
import tf

K_P = 0.4
K_D = 0.3125
look_ahead = 10
speed = 3
dist_point_reached = 3

coord_log_count = 250
log_counter = 0


def angle_diff(angle1, angle2):
    diff = angle1 - angle2
    if abs(diff) > pi:
        return copysign(2, diff) * pi - diff

    return diff


class StabilizedPController:
    def __init__(self):
        rospy.init_node('steering_p_controller')
        self.write_count = 0
        self.coord_file = open("/home/benjamin/Dokumente/Uni/robotics15-16/coord.txt", 'w')
        self.current_x = 0
        self.current_y = 0
        self.last_y = 0
        self.current_rot = 0
        self.last_rot = 0
        self.current_steer = 0
        self.last_steer = 0
        self.path = None
        self.log_counter = 0
        self.current_way_point = None
        self.publisher = rospy.Publisher('/ackermann_vehicle/ackermann_cmd', AckermannDriveStamped, queue_size=1)
        rospy.Subscriber("/ackermann_vehicle/odom", Odometry, lambda odometry: self.pos_update(odometry))
        rospy.Subscriber("/path", Path, lambda path: self.recv_path(path))

    def spin(self):
        rate = rospy.Rate(10)
        while not rospy.is_shutdown():
            self.calc_steering()
            rate.sleep()

    def calc_steering(self):
        if self.current_way_point is None:
            self.publish_steering(0, 0)
            return

        x_error = self.current_way_point.pose.position.x - self.current_x
        y_error = self.current_way_point.pose.position.y - self.current_y
        current_rot = self.current_rot

        # Bestimme den Winkel zum Wegpunkt
        if x_error < 0 < y_error:
            angle = atan(y_error / x_error) + pi
        elif x_error < 0 and y_error < 0:
            angle = atan(y_error / x_error) - pi
        elif x_error == 0:
            angle = copysign(1, y_error) * pi / 2
        else:
            angle = atan(y_error / x_error)

        # Berechne die benötigte Drehung zur Zielrotation
        delta_angle = angle_diff(angle, current_rot)

        # PD-Regler Berechnung
        u_t = K_P * delta_angle + K_D * (angle_diff(self.last_rot, current_rot))
        self.last_y = self.current_y
        self.last_rot = current_rot

        if self.log_counter % 5 == 0:
            self.coord_file.write(str(self.current_x) + ' ' + str(self.current_y) + '\n')

        self.log_counter += 1

        self.publish_steering(u_t, speed)

    def waypoint_dist(self):
        delta_x = self.current_x - self.current_way_point.pose.position.x
        delta_y = self.current_y - self.current_way_point.pose.position.y
        return sqrt(delta_x ** 2 + delta_y ** 2)

    def publish_steering(self, angle, speed):
        msg = AckermannDriveStamped()
        msg.header.stamp = rospy.Time(0)
        msg.drive.steering_angle = angle
        msg.drive.speed = speed
        msg.drive.acceleration = 3
        self.publisher.publish(msg)

    def pos_update(self, odometry):
        self.current_x = odometry.pose.pose.position.x
        self.current_y = odometry.pose.pose.position.y
        quaternion = (
            odometry.pose.pose.orientation.x,
            odometry.pose.pose.orientation.y,
            odometry.pose.pose.orientation.z,
            odometry.pose.pose.orientation.w
        )
        euler = tf.transformations.euler_from_quaternion(quaternion)
        self.current_rot = euler[2]

        # Neuer Pfad: Starte Navigation
        if self.path is not None and self.current_way_point is None:
            self.current_way_point = self.path.poses.pop(0)

        # Bestimmen des nächsten Wegpunktes (Überprüfung, ob aktueller Wegpunkt erreicht)
        while self.current_way_point is not None and self.waypoint_dist() < dist_point_reached:
            print("Waypoint reached. " + str(len(self.path.poses)) + " remaining.")
            if len(self.path.poses) > 0:
                self.current_way_point = self.path.poses.pop(0)
            else:
                # Sie haben Ihr Ziel erreicht
                self.current_way_point = None
                self.path = None

    def recv_path(self, path):
        print("Path received.")
        self.path = path


if __name__ == '__main__':
    crtl = StabilizedPController()
    crtl.spin()
