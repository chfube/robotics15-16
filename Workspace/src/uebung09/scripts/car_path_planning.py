#!/usr/bin/env python

from a_star import AStar
from way_node import WayNode, angle_diff
from math import pi, sqrt
import rospy
from nav_msgs.msg import Path
from geometry_msgs.msg import PoseStamped
import tf

# Kosten zwischen zwei Knoten ist immer die Schrittweite
def cost(node1, node2):
    return step


# Heuristik mit stärkerer Gewichtung auf dem Winkelunterschied
def estimate(node1, node2):
    return sqrt((node1.x - node2.x) ** 2 + (node1.y - node2.y) ** 2 + 2 * angle_diff(node1.rot, node2.rot) ** 2)


# Filtert die Nachfolger eines Knotens (Entfernt Knoten die im Hindernis liegen)
def succ_filter(successors):
    for node in successors[:]:
        if 15 <= node.x <= 20 and -5 <= node.y <= 20:
            successors.remove(node)


# Speichert die Koordinaten der Knoten in der angegebenen Datei
def save_as_gnu_plot(filepath, node_list):
    print(node_list)
    f = open(filepath, 'w')
    for node in node_list:
        f.write(str(node.x) + ' ' + str(node.y) + '\n')
    f.close()


# Wandelt einen AStern-Pfad in einen ROS-Pfad um
def convert_path(node_path):
    out_path = Path()
    poses = []
    for node in node_path:
        next_node = PoseStamped()
        quaternion = tf.transformations.quaternion_from_euler(0, 0, node.rot)
        next_node.pose.position.x = node.x
        next_node.pose.position.y = node.y

        next_node.pose.orientation.x = quaternion[0]
        next_node.pose.orientation.y = quaternion[1]
        next_node.pose.orientation.z = quaternion[2]
        next_node.pose.orientation.w = quaternion[3]

        poses.append(next_node)

    out_path.poses = poses
    out_path.header.stamp = rospy.Time(0)
    out_path.header.frame_id = 'map'
    return out_path


# Schrittgröße
step = pi

if __name__ == '__main__':
    pub = rospy.Publisher('/path', Path, queue_size=10, latch=True)
    path = '/home/benjamin/Dokumente/Uni/robotics15-16/'
    rospy.init_node('PathPlanner', anonymous=True)
    find = AStar(cost, estimate)

    # Pfad in Rviz anzeigen
    start_node = WayNode(0, 0, 0, step=step, radius=4, succ_filter=succ_filter)
    target_node = WayNode(30, 15, 0)
    pub.publish(convert_path(find.a_star(start_node, target_node)))
    rospy.spin()

    # Alle Pfade berechnen und in Datei schreiben
    target_node = WayNode(6, 3, pi / 2)
    save_as_gnu_plot(path + 'pathA.txt', find.a_star(start_node, target_node))
    target_node = WayNode(0, 5, 1.5 * pi)
    save_as_gnu_plot(path + 'pathB.txt', find.a_star(start_node, target_node))
    target_node = WayNode(0, 5, pi)
    save_as_gnu_plot(path + 'pathC.txt', find.a_star(start_node, target_node))
    target_node = WayNode(30, 15, 0)
    save_as_gnu_plot(path + 'pathD.txt', find.a_star(start_node, target_node))
