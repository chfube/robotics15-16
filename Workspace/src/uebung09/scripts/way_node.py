from math import pi, sin, cos, sqrt


def angle_diff(angle1, angle2):
    return min(abs(angle1 - angle2), 2 * pi - abs(angle1 - angle2))


class WayNode:
    def __init__(self, x, y, rot, step=pi, radius=4, succ_filter=None):
        self.x = x
        self.y = y
        self.rot = rot
        self.step = step
        self.step_rotation = step / radius
        self.radius = radius
        self.succ_filter = succ_filter

    # Definition der Nachfolger (Links, Geradeaus, Rechts)
    def successors(self):
        successors = [
            WayNode(self.x + self.radius * sin(self.step_rotation),  # x
                    self.y + self.radius - self.radius * cos(self.step_rotation),  # y
                    (self.rot + self.step_rotation) % (2 * pi),  # rot
                    step=self.step, radius=self.radius, succ_filter=self.succ_filter),

            WayNode(self.x + self.step * cos(self.rot),  # x
                    self.y + self.step * sin(self.rot),  # y
                    self.rot,  # rot
                    step=self.step, radius=self.radius, succ_filter=self.succ_filter),

            WayNode(self.x + self.radius * sin(-self.step_rotation),  # x
                    self.y + self.radius - self.radius * cos(-self.step_rotation),  # y
                    (self.rot - self.step_rotation) % (2 * pi),  # rot
                    step=self.step, radius=self.radius, succ_filter=self.succ_filter)
        ]

        if self.succ_filter is not None:
            self.succ_filter(successors)

        return successors

    # Gibt an, ob dieser Knoten ungefähr dem übergebenen Knoten entspricht
    def approx(self, node):
        return sqrt((self.x - node.x) ** 2 + (self.y - node.y) ** 2) <= 2.5 \
               and angle_diff(self.rot, node.rot) <= 1

    def __eq__(self, other):
        return self.x == other.x and self.y == other.y and self.rot == other.rot

    def __str__(self):
        return '(' + str(self.x) + ', ' + str(self.y) + ', ' + str(self.rot) + ')'

    def __repr__(self):
        return str(self)

    def __hash__(self):
        return hash(str(self))
